<?php

namespace Modules\Setting\Entities;

use App\Models\BaseModel;
use Spatie\Translatable\HasTranslations;

class City extends BaseModel
{
    use HasTranslations;

    protected $guarded = ['id'];
    public $translatable = ['name'];
    protected $casts = ['status' => 'boolean'];

}
