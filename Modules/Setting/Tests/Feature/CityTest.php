<?php

namespace Modules\Setting\Tests\Feature;

use Illuminate\Http\Response;
use Modules\Setting\Entities\City;
use Tests\InitTestCase;
use Tests\InitTestCaseInterface;

class CityTest extends InitTestCase implements InitTestCaseInterface
{
    private $base_url = 'api/setting-module-api/city';

    private $model;
    protected function setUp(): void
    {
        parent::setUp();
        $this->model = factory(City::class)->create();
    }

    public function testRequiredFieldsForCreation()
    {
        $this->json('POST', $this->base_url, [], $this->getHeaderData())
            ->assertStatus(400)
            ->assertJson([
                'code' => Response::HTTP_BAD_REQUEST,
                'data' => []
            ]);
    }

    /*
     * Relation Test
     */
//    public function a_user_has_a_phone()
//    {
//        $user = factory(City::class)->create();
//        $phone = factory(Phone::class)->create(['user_id' => $user->id]);
//
//        // Method 1:
//        $this->assertInstanceOf(Phone::class, $user->phone);
//
//        // Method 2:
//        $this->assertEquals(1, $user->phone->count());
//    }

    public function testListAll()
    {
        $this->json('GET', $this->base_url, [], $this->getHeaderData())
            ->assertStatus(200)
            ->assertJsonStructure([
                "code",
                "data" => [
                    "data",
                    "links",
                    "meta"
                ]
            ]);
    }

    public function testCreate()
    {
        $model = factory(City::class)->make();
        $request = $this->transformRequestData($model);
        $response = $this->json('POST', $this->base_url, $request, $this->getHeaderData());
        $response->assertStatus(Response::HTTP_CREATED)
            ->assertJson([
                'code' => Response::HTTP_CREATED,
                'message' => 'done.',
                'data' => [
                    'id' => $response['data']['id']
                ]
            ]);
    }


    public function testGeneralCase()
    {
        $model = factory(City::class)->make();
        $request = $this->transformRequestData($model);
        $response = $this->json('POST', $this->base_url, $request, $this->getHeaderData());

        $this->assertTrue($response->original['code'] == Response::HTTP_CREATED);
        $this->assertFalse($response->original['code'] == Response::HTTP_ACCEPTED);

        $response->assertStatus(Response::HTTP_CREATED)
            ->assertExactJson([
                'code' => Response::HTTP_CREATED,
                'message' => 'done.',
                'data' => [
                    'id' => $response['data']['id']
                ]
            ]);

        // for Debugging Responses
    //        $response->dump();
    }

    public function testShow()
    {
//        $model = factory(City::class)->create();
        $this->json('GET', $this->base_url . '/' . $this->model->id, [], $this->getHeaderData())
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'code' => Response::HTTP_OK,
                'message' => 'done.',
                'data' => [
//                    'id'     => $model->id,
                    'name' => [
                        'en' => $this->model->getTranslation('name', 'en'),
                        'ar' => $this->model->getTranslation('name', 'ar')
                    ],
                    'status' => $this->model->status,
                ]
            ]);
    }

    public function testUpdate()
    {
        $model = factory(City::class)->create();
        $request = $this->transformRequestData(factory(City::class)->make());
//        $this->assertDatabaseHas('cities', $request);
        $this->json('PUT', $this->base_url . '/' . $model->id, $request, $this->getHeaderData())
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'code' => Response::HTTP_OK,
                'message' => 'done.',
                'data' => [
                    'id' => $model->id
                ]
            ]);
    }

    public function testDelete()
    {
        $model = factory(City::class)->create();
        $this->json('DELETE', $this->base_url . '/' . $model->id, [], $this->getHeaderData())
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'code',
                'message'
            ]);
        $this->assertDatabaseMissing('cities',  $this->transformRequestData($model));
    }

    public function transformRequestData(\Illuminate\Database\Eloquent\Model $model): array
    {
        return [
            'name' => [
                'en' => $model->getAttribute('name')
            ],
            'status' => $model->getAttribute('status')
        ];
    }

}
