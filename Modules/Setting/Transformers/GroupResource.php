<?php

namespace Modules\Setting\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    public function toArray($request)
    {
        $items = [];
        switch ($request->route()->getActionMethod()) {
            case "index":
                $items = $this->getIndexData();
                break;
            case "show":
                $items = $this->getShowData();
                break;
            case "dropDownList":
                $items = $this->getDropDownData();
                break;
        }
        return $items;
    }

    private function getIndexData(): array
    {
        return [
            'id'     => $this->id,
            'name'   => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
            'status' => $this->status
        ];
    }

    private function getShowData(): array
    {
        return [
            'id'     => $this->id,
            'name'   => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
            'status' => $this->status
        ];
    }

    private function getDropDownData(): array
    {
        return [
            'id'   => $this->id,
            'name' => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
        ];
    }
}
