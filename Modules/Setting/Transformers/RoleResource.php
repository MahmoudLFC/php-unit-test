<?php

namespace Modules\Setting\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    public function toArray($request)
    {
        $response = [
            'id'     => $this->id,
            'status' => $this->status,
            'name'   => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')]
        ];
        switch ($request->route()->getActionMethod()) {
            case "index":
                $response['users_count'] = count($this->admins);
                break;
            case "show":
                $response['permissions'] = getPermissions(json_decode($this->permission));
                break;
            case "dropDownList":
                return $response = [
                    'id'   => $this->id,
                    'name' => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
                ];
                break;
        }
        return $response;
    }
}
