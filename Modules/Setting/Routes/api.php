<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => 'setting-module-api',
    'middleware' => 'auth:admin'
], function () {
    Route::apiResource('city', 'CityController');
    Route::GET('city/{city}/toggle-activation', 'CityController@toggleActivation');

});
