<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Setting\Entities\City;

$factory->define(City::class, function (Faker $faker) {
    return [
        'name'   => [
            'en' => $faker->name
        ],
        'status' => true
    ];
});
