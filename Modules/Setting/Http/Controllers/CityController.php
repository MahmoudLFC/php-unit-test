<?php

namespace Modules\Setting\Http\Controllers;

use App\Http\Controllers\Admin\InitController;
use App\Http\Helpers\Traits\ApiPaginator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Setting\Entities\City;
use Modules\Setting\Http\Requests\CityRequest;
use Modules\Setting\Transformers\CityResource;

class CityController extends InitController
{
    use ApiPaginator;

    public function index(Request $request)
    {
        $code = Response::HTTP_OK;
        $message = "done.";
        $data = [];
        try {
            $perPage = $request->per_page ? $request->per_page : 10;
            $query = City::query();
            $query = $this->filter($request, $query);
            $query = $query->paginate($perPage);
            $collection = CityResource::collection($query);
            $data = $this->getPaginatedResponse($query, $collection);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function store(CityRequest $request)
    {
        $code = Response::HTTP_CREATED;
        $message = "done.";
        $response = [];
        try {
            $data = $request->only(['name']);
            $model = City::query()->create($data);
            $response = ['id' => $model->id];
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $response);
    }

    public function show($id)
    {
        $code = Response::HTTP_OK;
        $message = "done.";
        $data = [];
        try {
            $item = City::query()->find($id);
            $data = new CityResource($item);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $data);
    }

    public function update(CityRequest $request, $id)
    {
        $code = Response::HTTP_OK;
        $message = "done.";
        $response = [];
        try {
            $data = $request->only(['name']);
            $model = City::query()->find($id);
            $model->update($data);
            $response = ['id' => $model->id];
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message,$response);
    }

    public function destroy($id)
    {
        $code = Response::HTTP_OK;
        $message = "done.";
        try {
            $city = City::query()->where('id', $id)->first();
            if (!$city) {
                throw new \Exception('item not found', Response::HTTP_NOT_FOUND);
            }
            $city->delete();
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }

    private function filter(Request $request, $query)
    {
        if ($request->has('status')) {
            $query = $query->where('status', $request->status);
        }
        if ($request->filled('keyword')) {
            $multiKeysSearch['key'] = $request->keyword;
            $multiKeysSearch['fields'] = [
                ['field' => 'name->en', 'like' => true],
                ['field' => 'name->ar', 'like' => true],
            ];
            $query = $query->filter($multiKeysSearch);
        }
        return $query;
    }

    public function toggleActivation(City $city)
    {
        $code = 200;
        $message = "done.";
        $response = [];
        try {
            $item = City::query()->where('id', $city->id)->first();
            $status = !$item->status;
            City::query()->update([
                'status' => $status
            ]);
            $response = [
                'id'     => $city->id,
                'status' => $status,
            ];
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $response);
    }
}
