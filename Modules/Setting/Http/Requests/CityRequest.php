<?php

namespace Modules\Setting\Http\Requests;

use App\Http\Requests\ResponseShape;

class CityRequest extends ResponseShape
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => 'array|required',
                        'name.*' => 'required|min:3',
                        'name.ar' => 'nullable|unique:cities,name->ar,NULL,id,deleted_at,NULL',
                        'name.en' => 'required|unique:cities,name->en,NULL,id,deleted_at,NULL',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'name' => 'array|required',
                        'name.*' => 'required|min:3',
                        'name.ar' => 'nullable|unique:cities,name->ar,'.$this->segment(4),
                        'name.en' => 'required|unique:cities,name->en,'.$this->segment(4),
                    ];
                }
            default:
                break;
        }
    }
}
