<?php

namespace Modules\Setting\Http\Requests;

use App\Http\Requests\ResponseShape;

class AreaRequest extends ResponseShape
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name.en' => 'required|unique:areas,name->en',
                    'name.ar' => 'unique:areas,name->ar',
                    'city_id' => 'required|exists:cities,id'
                ];
            }
            case 'PUT':
            {
                return [
                    'name.en' => 'required|unique:areas,name->en,'.$this->segment(4),
                    'name.ar' => 'unique:areas,name->ar,'.$this->segment(4),
                    'city_id' => 'required|exists:cities,id'
                ];
            }
            default:
                break;
        }
    }
}
