<?php

namespace Modules\Setting\Http\Requests;

use App\Http\Requests\ResponseShape;

class RoleRequest extends ResponseShape
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                $rules = [
                    'name.en'     => 'required|unique:roles,name->en',
                    'name.ar'     => 'unique:roles,name->ar',
                    'permissions' => 'required'
                ];
                return $rules;
            }
            case 'PUT':
            {
                $rules = [
                    'name.en'     => 'required|unique:roles,name->en,'.$this->segment(4),
                    'name.ar'     => 'unique:roles,name->ar,'.$this->segment(4),
                    'permissions' => 'required',
                ];
                return $rules;
            }
            default:
                break;
        }
    }
}
