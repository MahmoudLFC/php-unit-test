<?php

namespace Modules\Setting\Http\Requests;

use App\Http\Requests\ResponseShape;

class TaskTypeRequest extends ResponseShape
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name' => 'array|required',
                        'name.*' => 'required|min:3',
                        'name.ar' => 'nullable|unique:cities,name->ar',
                        'name.en' => 'required|unique:cities,name->en',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'name' => 'array|required',
                        'name.*' => 'required|min:3',
                        'name.ar' => 'nullable|unique:cities,name->ar,'.$this->segment(4),
                        'name.en' => 'required|unique:cities,name->en,'.$this->segment(4),
                    ];
                }
            default:
                break;
        }
    }
}
