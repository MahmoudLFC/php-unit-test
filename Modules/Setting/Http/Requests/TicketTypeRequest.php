<?php

namespace Modules\Setting\Http\Requests;

use App\Http\Requests\ResponseShape;

class TicketTypeRequest extends ResponseShape
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name'    => 'array|required',
                    'name.*'  => 'required|min:3',
                    'name.ar' => 'nullable|unique:ticket_types,name->ar',
                    'name.en' => 'required|unique:ticket_types,name->en',
                ];
            }
            case 'PUT':
            {
                return [
                    'name'    => 'array|required',
                    'name.*'  => 'required|min:3',
                    'name.ar' => 'nullable|unique:ticket_types,name->ar,'.$this->segment(4),
                    'name.en' => 'required|unique:ticket_types,name->en,'.$this->segment(4),
                ];
            }
            default:
                break;
        }
    }
}
