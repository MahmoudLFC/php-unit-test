<?php

namespace Modules\User\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Chain\Entities\Branch;
use Modules\Common\Entities\Media;
use Modules\Setting\Entities\Role;
use Spatie\Translatable\HasTranslations;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Demo extends Authenticatable implements JWTSubject
{
    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    use Notifiable;
    use HasTranslations;
    public $translatable = ['name'];
    protected $guarded = ['id'];
    protected $casts = ['status' => 'boolean'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function branchs()
    {
        return $this->belongsToMany(Branch::class,'demo_branches','demo_id','branch_id');
    }

    public function areas()
    {
        return $this->hasManyDeepFromRelations($this->branchs(), (new Branch())->area());
    }

    public function chains()
    {
        return $this->hasManyDeepFromRelations($this->branchs(), (new Branch())->chain());
    }
}
