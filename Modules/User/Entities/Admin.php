<?php

namespace Modules\User\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Chain\Entities\Branch;
use Modules\Setting\Entities\Area;
use Modules\Setting\Entities\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $guarded = ['id'];
    protected $fillable = [
        'name', 'email', 'phone', 'password', 'role_id', 'title', 'media_id'
    ];
    protected $casts = ['status' => 'boolean'];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function branchs()
    {
        return $this->belongsToMany(Branch::class, 'admin_branches', 'admin_id', 'branch_id');
    }
    public function areas()
    {
        return $this->belongsToMany(Area::class, 'admin_areas', 'admin_id', 'area_id');
    }
}
