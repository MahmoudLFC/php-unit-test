<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'user-module-api',
], function () {
    Route::post('login', ['as' => 'login', 'uses' => 'LoginController@login']);
});


