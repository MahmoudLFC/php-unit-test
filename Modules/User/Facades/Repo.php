<?php

namespace Modules\User\Facades;

class Repo
{

    public static function invoke()
    {
        return app()['Repo'];
    }

    public static function __callstatic($method, $args)
    {
        $instance = self::invoke();
        return $instance->$method(...$args);
    }
}