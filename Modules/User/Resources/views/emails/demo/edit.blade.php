<!DOCTYPE html>
<html>
<head>
    <title>Demo Email</title>
</head>
<body>
    <div>
        We hope you have a good day <br>
        Welcome to Zahran system. <br>
        You can now log in the system with the following credentials: <br>
        E-mail : {{$data['to']}} <br>
        Link: {{env('FRONT_URL', '---')}} <br>
        Thank you
    </div>
</body>
</html>
