@if($export_type == Modules\Common\Enums\ExportTypeEnum::PDF)
    <head>
        <style>
            @php include(base_path().'/Modules/Common/assets/css/bootstrap.min.css') @endphp
        </style>
    </head>
    <body>
    <header>
        <p style="font-weight:bold;font-size:20px;text-align: center">Demos Reports</p>
    </header>
    <main>
        <div class="col-md-12" style="margin-top:10px;">
            <div>
                @endif
                <table class="table table-bordered text-center">
                    @if($export_type == Modules\Common\Enums\ExportTypeEnum::Excel)
                        <tr>
                            <th colspan="6" style="font-size: 16px;text-align: center;font-weight: bold;padding: 10px;">Demos Report</th>
                        </tr>
                    @endif
                    <thead>
                    <tr style="background-color:#c4c4c4;color:#000">
                        <th style="vertical-align: middle;width: 10%;text-align: center">ID</th>
                        <th style="vertical-align: middle;width: 15%;text-align: center">Demo name</th>
                        <th style="vertical-align: middle;width: 20%;text-align: center">Branch</th>
                        <th style="vertical-align: middle;width: 20%;text-align: center">Area</th>
                        <th style="vertical-align: middle;width: 10%;text-align: center">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($items))
                        @foreach($items as $item)
                            <tr>
                                <td style="vertical-align: middle;text-align: center">{{$item->id}}</td>
                                <td style="vertical-align: middle;text-align: center">{{$item->name->en}}</td>
                                <td style="vertical-align: middle;text-align: center">{{implode('-',$item->branches)}}</td>
                                <td style="vertical-align: middle;text-align: center">{{implode('-',$item->areas)}}</td>
                                <td style="vertical-align: middle;text-align: center">{{$item->status ? 'Active' : 'Deactivated'}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if($export_type == Modules\Common\Enums\ExportTypeEnum::PDF)
            </div>
        </div>
    </main>
    </body>
@endif

