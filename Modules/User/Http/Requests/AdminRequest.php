<?php

namespace Modules\User\Http\Requests;
use App\Http\Requests\ResponseShape;
class AdminRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'role_id' => 'nullable|exists:roles,id',
                        'name' => 'required|min:2',
                        'email' => 'required|email|unique:admins,email',
                        'phone' => 'nullable|digits_between:9,14|unique:admins,phone',
                        'password' => 'nullable|confirmed|min:6',
                        'title'     => 'nullable|min:3',
                        'media_id'     => 'nullable|exists:media,id',
                    ];
                }
            case 'PUT':
                {
                    if(request()->wantsJson() && !request()->ajax()) {
                        return [
                            'role_id' => 'nullable|exists:roles,id',
                            'name' => 'required|min:2',
                            'email' => 'required|email|unique:admins,email,'.$this->segment(4),
                            'phone' => 'nullable|digits_between:9,14|unique:admins,phone,'.$this->segment(4),
                            'password' => 'nullable|confirmed|min:6',
                            'title'     => 'nullable|min:3',
                            'media_id'     => 'nullable|exists:media,id',
                        ];
                    } else {
                        return [
                            'role_id' => 'nullable|exists:roles,id',
                            'name' => 'required|min:2',
                            'email' => 'required|email|unique:admins,email,'.$this->segment(4),
                            'phone' => 'nullable|digits_between:9,14|unique:admins,phone,'.$this->segment(4),
                            'password' => 'nullable|confirmed|min:6',
                            'title'     => 'nullable|min:3',
                            'media_id'     => 'nullable|exists:media,id',
                        ];
                    }

                }
            default:
                break;
        }
    }
}
