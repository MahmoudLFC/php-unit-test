<?php

namespace Modules\User\Http\Requests;

use App\Http\Requests\ResponseShape;

class DemoRequest extends ResponseShape
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            {
                return [
                    'name.en'    => 'required|unique:demos,name->en',
                    'name.ar'    => 'nullable|unique:demos,name->ar',
                    'email'      => 'required|email|unique:demos,email',
                    'phone'      => 'nullable|digits_between:9,14|unique:demos,phone',
                    'password'   => 'nullable|confirmed|min:6',
                    'sab_number' => 'required|min:3|unique:demos,sab_number',
                    'media_id'   => 'nullable|exists:media,id',
                    'branch_ids' => 'required|array'
                ];
            }
            case 'PUT':
            {
                return [
                    'name.en'    => 'required|unique:demos,name->en'.$this->segment(4),
                    'name.ar'    => 'nullable|unique:demos,name->ar'.$this->segment(4),
                    'email'      => 'required|email|unique:demos,email,'.$this->segment(4),
                    'phone'      => 'nullable|digits_between:9,14|unique:demos,phone,'.$this->segment(4),
                    'password'   => 'nullable|confirmed|min:6',
                    'sab_number' => 'required|min:3|unique:demos,sab_number,'.$this->segment(4),
                    'media_id'   => 'nullable|exists:media,id',
                    'branch_ids' => 'required|array'
                ];
            }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'branch_ids.required' => 'the branch filed is required.',
        ];
    }
}
