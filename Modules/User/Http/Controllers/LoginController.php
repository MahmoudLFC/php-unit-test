<?php

namespace Modules\User\Http\Controllers;

use App\Http\Controllers\Admin\InitController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\Admin;
use Modules\User\Http\Requests\AdminRequest;
use Modules\User\Http\Requests\LoginRequest;
use Modules\User\Transformers\AdminsResource;

class LoginController extends InitController
{
    public function login(LoginRequest $request)
    {
        $code = 200;
        $message = "Logged in successfully.";
        $item = [];
        try {
            $credentials = $request->only(['email', 'password']);
            $credentials['status'] = 1;

            $item = Admin::where('email' , $request->email)->first();
            if (!$item['access_token'] = Auth::guard('admin')->attempt($credentials, true)) {
                throw new \Exception('Something went wrong!', 400);
                $item = [];
            }
            else{
                $item->method = 'login';
            }
            $item = new AdminsResource($item);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $item);
    }

    public function register(AdminRequest $request)
    {
        $code = 201;
        $message = "done.";
        $item = [];
        try {
            $data = $request->only(['name', 'email', 'phone', 'password', 'tilte']);
            $credentials = $request->only(['email', 'password']);
            $data['password'] = Hash::make($request->password);
            $item = Admin::create($data);
            if (!$item['access_token'] = Auth::guard('admin')->attempt($credentials, true)) {
                throw new \Exception('Something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $item);
    }

    function randomString($length = 8)
    {
        $str = "";
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public function profile(Request $request)
    {
        $code = 200;
        $message = "done.";
        $user = [];
        try {
            $user = Auth::guard('admin')->user();
            unset($user['role']);
        } catch (\Exception $e) {
            $code = getCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, $user);
    }



}
