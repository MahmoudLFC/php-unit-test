<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Chain\Entities\Branch;
use Modules\Common\Transformers\MediaResource;
use Modules\Setting\Entities\Area;

class DemosResource extends JsonResource
{
    public function toArray($request)
    {
        switch ($request->route()->getActionMethod()) {
            case "index":
                $items = $this->getIndexData();
                break;
            case "show":
                $items = $this->getShowData();
                break;
            default:
                $items = $this->getDropDownData();
                break;
        }
        return $items;
    }

    private function getIndexData(): array
    {
        $branch_ids = $this->branchs->pluck('id');
        $area_ids = Branch::query()->select('area_id')->whereIn('id', $branch_ids)->pluck('area_id')->unique()->toArray();
        return [
            'id'         => $this->id,
            'name'       => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
            'email'      => $this->email,
            'phone'      => $this->phone,
            'sab_number' => $this->sab_number,
            'branches'   => $this->branchs->pluck('name')->unique()->toArray(),
            'areas'      => Area::query()->whereIn('id', $area_ids)->pluck('name')->unique()->toArray(),
            'status'     => $this->status,
        ];
    }

    private function getDropDownData(): array
    {
        return [
            'id'   => $this->id,
            'name' => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
        ];
    }

    private function getShowData(): array
    {
        $branch_ids = $this->branchs->pluck('id')->toArray();
        $area_ids = Branch::query()->select('area_id')->whereIn('id', $branch_ids)->pluck('area_id')->unique()->toArray();
        $city_ids = Area::query()->select('city_id')->whereIn('id', $area_ids)->pluck('city_id')->unique()->toArray();
        return [
            'id'         => $this->id,
            'name'       => ['ar' => $this->getTranslation('name', 'ar'), 'en' => $this->getTranslation('name', 'en')],
            'email'      => $this->email,
            'phone'      => $this->phone,
            'sab_number' => $this->sab_number,
            'media'      => new MediaResource(optional($this->media)),
            'city_ids'   => $city_ids,
            'area_ids'   => $area_ids,
            'branch_ids' => $branch_ids,
            'status'     => $this->status,
        ];
    }
}
