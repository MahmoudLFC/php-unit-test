<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminsResource extends JsonResource
{
    public function toArray($request)
    {
        $result= [
            'id'     => $this->id,
            'name'   => $this->name,
            'email'   => $this->email,
            'phone'   => $this->phone,
            'title'   => $this->title,
            'status' => $this->status,
        ];
        if($this->method == 'login'){
            $result['access_token'] = $this->access_token;
        }
        return $result;
    }
}
