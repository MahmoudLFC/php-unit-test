<?php

namespace Modules\User\Tests\Feature;

use Tests\InitTestCase;

class AuthenticationTest extends InitTestCase
{
    private $base_url = 'api/user-module-api/login';

    public function testMustEnterEmail()
    {
        $this->json('POST', $this->base_url)->assertStatus(400)->assertJson([
            "message" => "The email field is required."
        ]);
    }

    public function testSuccessfulLogin()
    {
        $loginData = ['email' => 'dev.mahmoud2012@gmail.com', 'password' => '123456'];
//       $loginData = ['email' => 'dev.mahmoud2014@gmail.com', 'password' => '123456'];

        $res = $this->json('POST', $this->base_url, $loginData, ['Accept' => 'application/json'])
             ->assertStatus(200)
             ->assertJsonStructure([
                'code',
                'message',
                'data' => [
                    'id',
                    'name',
                    'email',
                    'phone',
                    'title',
                    'status',
                    "access_token"
                ]
             ]);
        $this->setAccessToken($res['data']['access_token']) ;
    }

}
