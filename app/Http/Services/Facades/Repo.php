<?php

namespace App\Http\Services\Facades;

class Repo
{
    public static function invoke()
    {
        return app()['Repo'];
    }

    public static function __callStatic($method, $args)
    {
        $instance = self::invoke();
        return $instance->$method(...$args);
    }
}
