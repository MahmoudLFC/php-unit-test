<?php

namespace App\Http\Services;

/**
 *  Tarek Mahfouz
 */
class ServiceRepository
{
    private static $namespace = 'Modules\\';

    private $model;

    public function getAll($model, $args = [])
    {
        $moduleName = isset($args['moduleName']) ? $args['moduleName'] : '';
        $class = self::$namespace."$moduleName\\Entities\\$model";
        $this->model = new $class();
        $conditions = isset($args['conditions']) ? $args['conditions'] : [];
        $orWhere = isset($args['orWhere']) ? $args['orWhere'] : [];
        $whereIn = isset($args['whereIn']) ? $args['whereIn'] : [];
        $with = isset($args['with']) ? $args['with'] : [];
        $whereBetween = isset($args['whereBetween']) ? $args['whereBetween'] : [];
        $multiKeysSearch = isset($args['multiKeysSearch']) ? $args['multiKeysSearch'] : [];
        $WhereHas = isset($args['WhereHas']) ? $args['WhereHas'] : [];
        $orderBy = isset($args['orderBy']) ? $args['orderBy'] : null;
        $take = isset($args['take']) ? $args['take'] : null;
        $reverse = isset($args['reverse']) ? $args['reverse'] : false;
        $per_page = isset($args['per_page']) ? $args['per_page'] : null;

        /*
            With Sample: (Get Data with Relations)
            $with = ['user', 'category'];
        */
        $items = count($with) ? $this->model->with($with) : $this->model;

        /*
            Conditions Sample:
            $conditions = [
                'status' => 1,
                ['price','>',1000]
            ];
        */
        $items = $items->where($conditions);

        /*
            Sample:
            // Without Search In Relation
            $WhereHas = [
                'relation' => 'user',
                'conditions' => [
                    'status' => 1,
                    ['age' ,'>', 25]
                ],
            ],
            // With Search In Relation
            $WhereHas = [
                'relation' => 'user',
                'withSearch' => [
                    'attributes' => ['username','email','first_name'],
                    'keyword' => 'America'
                ]
            ];
        */
        if (count($WhereHas)) {
            if (isset($WhereHas['withSearch'])) {
                $withSearch = $WhereHas['withSearch'];
                if (isset($withSearch['attributes']) && isset($withSearch['keyword'])) {
                    $items = $items->WhereHas($WhereHas['relation'], function ($sql) use ($withSearch) {
                        $sql->where($withSearch['attributes'][0], 'LIKE', '%'.$withSearch['keyword'].'%');
                        foreach ($withSearch['attributes'] as $index => $attribute) {
                            if ($index < count($withSearch['attributes']) - 1) {
                                $sql->orWhere($withSearch['attributes'][$index + 1], 'LIKE', '%'.$withSearch['keyword'].'%');
                            }
                        }
                    });
                }
            } else {
                if (isset($WhereHas['conditions'])) {
                    $items = $items->whereHas($WhereHas['relation'], function ($sql) use ($WhereHas) {
                        $sql->where($WhereHas['conditions']);
                    });
                }
            }
        }

        /*
            Where-Between Sample:
            $whereBetween = [
                'attribute' => 'created_at',
                'from' => 'datetime',
                'to' => 'datetime',
            ];
        */
        if (count($whereBetween)) {
            if (isset($whereBetween['attribute']) && isset($whereBetween['from']) && isset($whereBetween['to'])) {
                $items = $items->whereBetween($whereBetween['attribute'], [$whereBetween['from'], $whereBetween['to']]);
            }
        }

        /*
            WhereIn Sample:
            $whereIn = [
                'key' => 'id',
                'values' => [1,2,3,4,5],
            ];
        */
        if (count($whereIn)) {
            foreach ($whereIn as $key => $item) {
                if (isset($item['key']) && isset($item['values'])) {
                    $items = $items->whereIn($item['key'], $item['values']);
                }
            }
        }

        /*
            Multi-Keys Search Sample:
            $multiKeysSearch = [
                'key' => 'asd',
                'fields' => [
                    ['field'=>'name_en', 'like'=>true],
                    ['field'=>'name_ar', 'like'=>true],
                ]
            ];
        */
        if (count($multiKeysSearch)) {
            $items = $items->where(function ($sql) use ($multiKeysSearch) {
                if ($multiKeysSearch['fields'][0]['like']) {
                    $sql->where($multiKeysSearch['fields'][0]['field'], 'LIKE', '%'.$multiKeysSearch['key'].'%');
                } else {
                    $sql->where($multiKeysSearch['fields'][0]['field'], $multiKeysSearch['key']);
                }
                foreach ($multiKeysSearch['fields'] as $index => $field) {
                    if ($index < count($multiKeysSearch['fields']) - 1) {
                        if ($multiKeysSearch['fields'][$index + 1]['like']) {
                            $sql->orWhere($multiKeysSearch['fields'][$index + 1]['field'], 'LIKE', '%'.$multiKeysSearch['key'].'%');
                        } else {
                            $sql->orWhere($multiKeysSearch['fields'][$index + 1]['field'], $multiKeysSearch['key']);
                        }
                    }
                }
            });
        }

        /*
            OrWhere Sample:
            $orWhere = [
                'status' => 1,
                ['discount','>',5],
                ['price','>',3902],
            ];
            Note: orWhere Will be called if previous situations don't fetch any data
        */
        $items = count($orWhere) ? $items = $items->orWhere($orWhere) : $items;

        /*
            orderBy Sample:  'price'        // Order Response By Price
            reverse Sample:  True / False   // Reverse Response List
            take Sample:  10                // Take 10 Items Only From Response
            per_page Sample:  10            // Paginate Response 10 Items Per Page
        */
        $items = $orderBy ? ($reverse ? $items->orderBy($orderBy, 'DESC') : $items->orderBy($orderBy, 'ASC')) : ($reverse ? $items->orderBy('id', 'DESC') : $items->orderBy('id', 'ASC'));

        $items = $take ? $items->take($take) : $items;
        //return $items->toSql();
        $items = $per_page ? $items->paginate($per_page) : $items->get();
        return $items;
    }

    public function find($model, $args = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $conditions = isset($args['conditions']) ? $args['conditions'] : [];
        $orWhere = isset($args['orWhere']) ? $args['orWhere'] : [];
        $with = isset($args['with']) ? $args['with'] : [];

        $item = count($with) ? $this->model->with($with) : $this->model;
        if (count($orWhere)) {
            $item = $item->where($conditions)->orWhere($orWhere)->first();
        } else {
            $item = $item->where($conditions)->first();
        }

        return $item;
    }

    public function getOne($model, $args = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $conditions = isset($args['conditions']) ? $args['conditions'] : [];
        $orWhere = isset($args['orWhere']) ? $args['orWhere'] : [];
        $with = isset($args['with']) ? $args['with'] : [];
        $last = isset($args['last']) ? $args['last'] : true;
        $order_by = isset($args['order_by']) ? $args['order_by'] : 'created_at';

        $item = count($with) ? $this->model->with($with) : $this->model;
        if (count($orWhere)) {
            $item = $item->where($conditions)->orWhere($orWhere)->first();
        } else {
            $item = $item->where($conditions)->first();
        }

        $item = $last ? $item->orderBy($order_by, 'DESC')->first() : $item->orderBy($order_by, 'ASC')->first();

        return $item;
    }

    public function create($model, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->create($data);
        return $item;
    }

    public function bulkInsert($model, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = $this->model->insert($data);
        return $items ? 'OK' : 'Error';
    }

    public function updateOrCreate($model, array $conditions, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->updateOrCreate($conditions, $data);
        return $item;
    }

    public function update($model, $conditions, $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $this->model->where($conditions)->update($data);
        $item = $this->model->where($conditions)->first();
        return $item;
    }

    public function updateMuiltiple($model, $args)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $conditions = isset($args['conditions']) ? $args['conditions'] : [];
        $whereIn = isset($args['whereIn']) ? $args['whereIn'] : [];
        $data = isset($args['data']) ? $args['data'] : [];

        if (count($conditions)) {
            $items = $this->model->where($conditions);
        }
        if (count($whereIn)) {
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        }
        $items->update($data);

        return true;
    }

    public function saveTranslation($model, $data = [], $translationData = [], $add = true)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        if (!$add) {
            $item = $this->model->where($data)->first();
        } else {
            $item = $this->model->create($data);
        }
        foreach ($translationData as $td) {
            $item->translateOrNew($td['lang'])[$td['field']] = $td['value'];
            $item->save();
        }
        return $item;
    }

    public function destroy($model, $args = [])
    {
        $class = self::$namespace.$model;
        $items = $this->model = new $class();

        $conditions = isset($args['conditions']) ? $args['conditions'] : [];
        $whereIn = isset($args['whereIn']) ? $args['whereIn'] : [];
        $relations = isset($args['relations']) ? $args['relations'] : [];

        if ((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && !count($conditions)) {
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        } elseif (!count($whereIn) && count($conditions)) {
            $items = $this->model->where($conditions);
        } elseif ((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && count($conditions)) {
            $items = $this->model->where($conditions)->whereIn($whereIn['key'], $whereIn['values']);
        }
        //$this->model->where($condition)->whereIn($key, $whereIn)->delete();
        $items = $items->get();

        foreach ($items as $item) {
            if (count($relations)) {
                foreach ($relations as $relation) {
                    $item->$relation()->delete();
                }
            }
            $item->delete();
        }
        return true;
    }

}
