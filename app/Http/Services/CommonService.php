<?php

namespace App\Http\Services;

/**
 *  Tarek Mahfouz
 */
class CommonService
{
    private $model;
    //private static $namespace = 'App\Models\\';
    private static $namespace = "Modules\\";

    public function getAll($model, $conditions = [], $with = [], $per_page = 0, $whereIn = [], $reverse = true, $multiKeysSearch = [], $orderBy = null, $take = 0, $orWhere = [], $whereHas = [], $orwhereHas = [], $multiWhereIn = [], $whereBetween = [], $whereCustom = [], $moduleName = '')
    {
        $class = self::$namespace."Entities\\$moduleName\\$model";
        $this->model = new $class();

        $items = count($with) ? $this->model->with($with) : $this->model;
        if (count($orWhere)) {
            $items = $items->where($conditions)->orWhere($orWhere);
        } else {
            $items = $items->where($conditions);
        }
        if (count($orwhereHas)) {
            $searchValue = $orwhereHas['key'];
            foreach ($orwhereHas['relation'] as $key => $value) {
                $items = $items->WhereHas($value['name'], function ($sql) use ($value, $searchValue) {
                    foreach ($value['keyes'] as $key) {
                        $sql->orWhere($key, 'LIKE', '%'.$searchValue.'%');
                    }
                });
            }
        }
        $items = (count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $items->whereIn($whereIn['key'], $whereIn['values']) : $items;

        $items = count($whereBetween) ? $items->whereBetween($whereBetween['date'], [$whereBetween['from'], $whereBetween['to']]) : $items;
        if (count($multiWhereIn)) {
            foreach ($multiWhereIn as $key => $WhereInArr) {
                $items = $items->whereIn($WhereInArr['key'], $WhereInArr['values']);
            }
        }
        /*
            Multi-Keys Search Example

            $multiKeysSearch['key'] = $request->keyword;
            $multiKeysSearch['fields'] = [
                ['field'=>'name_en', 'like'=>true],
                ['field'=>'name_ar', 'like'=>true],
            ];
        */
        if (count($multiKeysSearch)) {
            $items = $items->where(function ($sql) use ($multiKeysSearch) {
                if ($multiKeysSearch['fields'][0]['like']) {
                    $sql->where($multiKeysSearch['fields'][0]['field'], 'LIKE', '%'.$multiKeysSearch['key'].'%');
                } else {
                    $sql->where($multiKeysSearch['fields'][0]['field'], $multiKeysSearch['key']);
                }
                foreach ($multiKeysSearch['fields'] as $field) {
                    if ($field['like']) {
                        $sql->orWhere($field['field'], 'LIKE', '%'.$multiKeysSearch['key'].'%');
                    } else {
                        $sql->orWhere($field['field'], $multiKeysSearch['key']);
                    }
                }
            });
        }
        if (count($whereHas)) {
            foreach ($whereHas as $relation) {
                $items = $items->whereHas($relation['relation'], function ($sql) use ($relation) {
                    if ($relation['relation'] == 'subBrands') {

                        $sql->whereIn('brands.'.$relation['key'], $relation['values']);
                    } elseif ($relation['relation'] == 'visit' || $relation['relation'] == 'role' || $relation['relation'] == 'task') {
                        $sql->whereIn($relation['relation'].'s.'.$relation['key'], $relation['values']);

                    } elseif ($relation['relation'] == 'bonuses') {
                        $sql->whereIn('bonusables.'.$relation['key'], $relation['values']);

                    } else {
                        $sql->whereIn($relation['relation'].'.'.$relation['key'], $relation['values']);

                    }
                });
            }
        }

        $items = $orderBy ? ($reverse ? $items->orderBy($orderBy, 'DESC') : $items->orderBy($orderBy, 'ASC')) : ($reverse ? $items->orderBy('id', 'DESC') : $items->orderBy('id', 'ASC'));

        //return $items->toSql();
        $items = $take ? $items->take($take) : $items;
        //return $items->toSql();
        $items = $per_page ? $items->paginate($per_page) : $items->get();

        return $items;
    }

    public function find($model, $conditions = [], $with = [], $orWhere = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        if (count($orWhere)) {
            $item = $item->where($conditions)->orWhere($orWhere)->first();
        } else {
            $item = $item->where($conditions)->first();
        }

        return $item;
    }

    public function findOrCreate($model, $conditions = [], $data = [], $with = [], $orWhere = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        if (count($orWhere)) {
            $item = $item->where($conditions)->orWhere($orWhere)->first();
        } else {
            $item = $item->where($conditions)->first();
        }
        if (!$item) {
            $item = $this->create($model, $data);
        }

        return $item;
    }

    public function getOne($model, $conditions = [], $with = [], $last = false)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        $item = count($with) ? $this->model->with($with) : $this->model;
        $item = $item->where($conditions);
        $item = $last ? $item->orderBy('created_at', 'DESC')->first() : $item->orderBy('created_at', 'ASC')->first();
        return $item;
    }

    public function create($model, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->create($data);
        return $item;
    }

    public function updateOrCreate($model, array $conditions, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->updateOrCreate($conditions, $data);
        return $item;
    }

    public function bulkInsert($model, array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = $this->model->insert($data);
        return $items ? 'OK' : 'Error';
    }

    public function upsert($model, array $data, array $columns_to_check, array $columns_to_update)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = $this->model->upsert($data, $columns_to_check, $columns_to_update);
        return $items ? 'OK' : 'Error';
    }

    public function update($model, $condition, $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        $item = $this->model->where($condition)->first();
        if ($item) {
            $item->update($data);
        }
        return $item;
    }

    public function updateMuiltiple($model, $condition = [], $data, $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        if (count($condition)) {
            $items = $this->model->where($condition);
        }
        if (count($whereIn)) {
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        }
        $items->update($data);

        return true;
    }

    public function updateOrCreateMuiltiple($model, $data, $condition = [], $whereIn = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        if (count($condition)) {
            $items = $this->model->where($condition);
        }
        if (count($whereIn)) {
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        }
        if (count($items)) {
            $items->update($data);
        } else {
            $this->bulkInsert($this->model, $data);
        }
        return true;
    }

    public function saveTranslation($model, $data = [], $translationData = [], $add = true)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        if (!$add) {
            $item = $this->model->where($data)->first();
        } else {
            $item = $this->model->create($data);
        }
        foreach ($translationData as $td) {
            $item->translateOrNew($td['lang'])[$td['field']] = $td['value'];
            $item->save();
        }
        return $item;
    }

    public function destroy($model, $condition = [], $whereIn = [], $relations = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        $items = $this->model;
        if ((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && !count($condition)) {
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        } elseif (!count($whereIn) && count($condition)) {
            $items = $this->model->where($condition);
        } elseif ((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && count($condition)) {
            $items = $this->model->where($condition)->whereIn($whereIn['key'], $whereIn['values']);
        }
        //$this->model->where($condition)->whereIn($key, $whereIn)->delete();
        $items = $items->get();

        foreach ($items as $item) {
            $item->delete();
        }
        return true;
    }

}
