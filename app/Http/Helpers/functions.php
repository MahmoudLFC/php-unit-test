<?php

use App\Http\Services\CommonService;
use Dompdf\Dompdf;
use Illuminate\Support\Str;


function jsonResponse($code = 200, $message = '', $data = [])
{
    return response()->json([
        'code'    => $code,
        'message' => $message,
        'data'    => $data ?? [],
    ], $code);
}

function uploadFile($file, $path, $edit = false, $oldFile = null)
{
    $destination = env('SYSTEM_PATH')().'/'.$path;
    $oldDestination = env('SYSTEM_PATH')().'/'.$path.'/'.$oldFile;
    if ($edit && is_file($oldDestination)) {
        $name = explode('.', $oldFile)[0];
        if ($name != 'default') {
            unlink($oldDestination);
        }
    }
    $ext = $file->getClientOriginalExtension();
    $name = time().Str::random(5);
    $fileName = $name.'.'.$ext;
    $file->move($destination, $fileName);
    return $fileName;
}

function generateCode($model, $count, $numbersOnly = false)
{
    $serviceObj = new CommonService;
    $code = $numbersOnly ? rand(((int)$count - 1) * 10, pow(10, (int)$count) - 1) : strToUpper(Str::random(((int)$count)));
    $exist = $serviceObj->find($model, ['code' => $code]);
    if ($exist) {
        generateCode();
    }
    return $code;
}


function getCode($code)
{
    $list = [501, 500, 415, 412, 406, 405, 404, 403, 401, 400, 307, 304, 303, 302, 301, 204, 202, 201, 200,];
    return in_array($code, $list) ? $code : 400;
}


function FCMPush($title, $body, $type, $token = null, $topic = null, $extra = [])
{
    $url = 'https://fcm.googleapis.com/fcm/send';
    $data = [
        "title"             => $title,
        "body"              => $body,
        "type"              => $type,
        //"data"  => $extra,
        'content_available' => true,
        'vibrate'           => 1,
        'sound'             => true,
        'priority'          => 'high',
    ];
    foreach ($extra as $key => $value) {
        $data[$key] = $value;
    }

    if ($topic != null) {
        $fields = [
            'to'           => '/topics/'.$topic,
            'notification' => $data,
            'priority'     => 'high'
        ];
    } else {
        $fields = [
            'to'           => $token,
            'notification' => $data,
            'priority'     => 'high'
        ];
    }
    $fcmApiKey = env('FIREBASE_TOKEN');
    $headers = [
        'Authorization: key='.$fcmApiKey,
        'Content-Type:application/json'
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === false) {
        die('cUrl faild: '.curl_error($ch));
    }
    curl_close($ch);
    return $result;
}


function lang($keyword)
{
    return trans('lang.'.$keyword);
}


function getDays()
{
    return (object)[
        'names' => ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',],
        'codes' => ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
    ];
}

function geStartDateEndDateOfCurrentWeek()
{

    $first_day_of_the_week = 'Saturday';
    $start_of_the_week = strtotime("Last $first_day_of_the_week");
    if (strtolower(date('l')) === strtolower($first_day_of_the_week)) {
        $start_of_the_week = strtotime('today');
    }
    $end_of_the_week = $start_of_the_week + (60 * 60 * 24 * 7) - 1;

    //$date_format = 'l jS \of F Y h:i:s A';
    $date_format = 'Y-m-d';

    // This prints out Sunday 7th of December 2014 12:00:00 AM
    // See PHP: date - Manual
    // for ways to format the date
    $obj = new stdClass();
    $obj->start_of_the_week = date($date_format, $start_of_the_week);
    // This prints out Saturday 13th of December 2014 11:59:59 PM
    $obj->end_of_the_week = date($date_format, $end_of_the_week);
    return $obj;
}

function checkIfDateBetweenTwoDates($start, $end, $date)
{
    $compareableDate = date('Y-m-d', strtotime($date));
    $dateBegin = date('Y-m-d', strtotime($start));
    $dateEnd = date('Y-m-d', strtotime($end));
    if (($compareableDate >= $dateBegin) && ($compareableDate <= $dateEnd)) {
        return true;
    } else {
        return false;
    }
}

function permissions($roles)
{
    $list = [
        "system"         => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
        ],
        "users"          => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
        ],
        "chains"         => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
        ],
        "branches"       => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
        ],
        "brands"         => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
        ],
        "tasks"          => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true
        ],
        "demos"  => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
            "assign" => true,
        ],
        "tickets"        => [
            "view"   => true,
            "show"   => true,
            "add"    => true,
            "delete" => true
        ],
        "visits"         => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true,
            "assign" => true,
        ],
        "promotions"     => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true
        ],
        "roles"          => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true
        ],
        "bonus_criteria" => [
            "view" => true,
            "add"  => true
        ],
        "bonuses"        => [
            "view"                   => true,
            "add_merchandiser_bonus" => true,
            "add_supervisor_bonus"   => true,
            "approve"              => true
        ],
        "products"       => [
            "view"   => true,
            "edit"   => true,
            "add"    => true,
            "delete" => true
        ],
        "feed"           => [
            "view" => true
        ],
        "messages"       => [
            "view"   => true,
            "show"   => true,
            "add"    => true,
            "delete" => true
        ],
        "dashboard"      => [
            "view" => true
        ],
        "settings"       => [
            "view" => true
        ],
        "reports"        => [
            "view"          => true,
            "tickets"       => true,
            "visits"        => true,
            "merchandisers" => true
        ],
    ];

    $objected = $keys = [];
    foreach ($roles as $rolePermission) {
        $key = explode('.', $rolePermission)[0];
        $keys[] = $key;
    }
    $keys = array_unique($keys);

    foreach ($list as $key => $value) {
        $permissionsOptionsKeys = array_keys($value);
        //$key = 'system_values';
        //permissionsOptionsKeys = [ "view", "edit" ];
        foreach ($permissionsOptionsKeys as $permissionsOptionsKey) {
            if (in_array($key.'.'.$permissionsOptionsKey, $roles)) {
                $objected[$key][$permissionsOptionsKey] = true;
            } else {
                $objected[$key][$permissionsOptionsKey] = false;
            }
        }
    }
    foreach ($objected as $object) {
        $object = (object)$object;
    }
    return (object)$objected;
}

function getPermissions($permissions)
{
    $result = [];
    $permissions = (array)$permissions;
    $permissionsListKeys = array_keys($permissions);
    foreach ($permissionsListKeys as $key) {
        $permission = (array)($permissions[$key]);
        $item = [];
        foreach ($permission as $Itemkey => $permissionAccessability) {
            if ($permissionAccessability) {
                $result[] = $key.'.'.$Itemkey;
            }
        }
    }
    return $result;
}

if (!function_exists('generatePdf')) {
    function generatePdf($data)
    {
        $html = view($data['template']['path'], $data)->render();
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        return $dompdf->output();
    }
}


