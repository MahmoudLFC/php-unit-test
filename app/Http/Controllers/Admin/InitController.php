<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Carbon\Carbon;

class InitController extends Controller
{
    public $serviceObj;
    public $data;
    public $user;

    public function __construct()
    {
        app()->setLocale('en');
        Carbon::setLocale('en');
        $this->serviceObj = new CommonService;
    }
}
