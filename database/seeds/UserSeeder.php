<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Modules\User\Entities\Admin::updateOrCreate(['id' => 1],
            ['name' => 'Mahmoud Abdelsamad', 'email' => 'dev.mahmoud2012@gmail.com', 'phone' => '+201027670928',
                'password' => Hash::make('123456') , 'title' => 'Team Leader', 'status' => 1]);
    }
}
